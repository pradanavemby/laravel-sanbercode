<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "questions";

    protected $fillable = ['title', 'content']; // ini nentuin kolom mana yang bisa diisi
    protected $guarded = []; // ini nentuin kolom mana yang gak bisa diisi, kalo kosong berarti semua kolom bisa diisi
}
