<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // ini cara query builder
        //$questions = DB::table('questions')->get();

        //dd($quesions);

        // ini cara Eloquent ORM   
        $questions = Pertanyaan::all();

        return view('pertanyaan.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pertanyaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'title' => 'required|unique:questions',
            'content' => 'required'
        ]);

        // ini cara query builder
        /* $query = DB::table('questions')
                    ->insert([
                        "title" => $request["title"],
                        "content" => $request["content"]
                    ]); */

        // ini cara 1 1 masukinnya (Eloquent ORM   )
        /* $question = new Pertanyaan;
        $question->title = $request['title'];  
        $question->content = $request['content'];  
        $question->save(); */

        // ini cara mass assignment (Eloquent ORM   )
        $question = Pertanyaan::create([
            "title" => $request["title"],
            "content" => $request["content"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // ini cara query builder
        /* $question = DB::table('questions')
                       ->where('id', $id)
                       ->first(); */

        // ini cara Eloquent ORM         
        $question = Pertanyaan::find($id);

        return view('pertanyaan.show', compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // ini cara query builder
        /* $question = DB::table('questions')
                       ->where('id', $id)
                       ->first(); */
                       
        // ini cara Eloquent ORM  
        $question = Pertanyaan::find($id);

        return view('pertanyaan.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // ini cara query builder
        /* $query = DB::table('questions')
                    ->where('id', $id)
                    ->update([
                        'title' => $request['title'], 
                        'content' => $request['content']
                    ]); */

        // ini cara mass update (Eloquent ORM)
        $update = Pertanyaan::where('id', $id)
                  ->update([
                    "title" => $request["title"],
                    "content" => $request["content"]
                  ]);

        return redirect('pertanyaan')->with('success', 'Pertanyaan berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // ini cara query builder
        /* $query = DB::table('questions')
                    ->where('id', $id)
                    ->delete(); */

        // ini cara Eloquent ORM
        Pertanyaan::destroy($id);   

        return redirect('pertanyaan')->with('success', 'Pertanyaan berhasil dihapus');
    }
}