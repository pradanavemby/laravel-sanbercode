<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Daftar</title>
    </head>
    <body>
        <form action="/welcome" method="POST">
            @csrf
            <h1>Buat Account Baru!</h1>
            <h3>Sign Up Form</h3>

            <label for="fname">First name:</label>
            <br>
            <input type="text" id="fname" name="fname"> 

            <br><br>

            <label for="lname">Last name:</label>
            <br>
            <input type="text" id="lname" name="lname"> 
        
            <br><br>

            <label for="gender">Gender:</label>
            <br>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label>
            
            <br><br>

            <label for="nationality">Nationality:</label>
            <br>
            <select id="nationality" name="nationality">
                <option value="indonesian">Indonesian</option>
                <option value="malaysian">Malaysian</option>
                <option value="singaporean">Singaporean</option>
            </select>

            <br><br>

            <label for="language">Language Spoken:</label>
            <br>
            <input type="checkbox" id="language" name="language" value="bahasaIndonesia">
            <label for="bahasaIndonesia">Bahasa Indonesia</label><br>
            <input type="checkbox" id="language" name="language" value="english">
            <label for="english">English</label><br>
            <input type="checkbox" id="language" name="language" value="other">
            <label for="other">Other</label>

            <br><br>

            <label for="bio">Bio:</label>
            <br>
            <textarea id="bio" name="bio" rows="5" cols="30"></textarea>

            <br><br>

            <button type="submit">Sign Up</button>
           
        </form>      
    </body>
</html>