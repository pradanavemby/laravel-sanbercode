@extends('layouts.master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Buat Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="title" name="title" value=" {{ old('title', '') }} " placeholder="Masukkan judul">
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="content">Isi</label>
                    <input type="text" class="form-control" id="content" name="content" value=" {{ old('content', '') }} " placeholder="Masukkan pertanyaan">
                    @error('content')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Buat</button>
            </div>
        </form>
    </div>

@endsection